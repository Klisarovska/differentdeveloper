
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Different Developer</title>
        <meta name = "keywords" content = "Webpage development, app development, PHP, Wordpress, Laravel, Custom" />
        <meta name = "description" content = "Custom web app and website developemnt" />
        <meta name = "author" content = "Elena Klisarovska" />
        <link rel="stylesheet" href="assets/css/styles.css">

        <!-- =====BOX ICONS===== -->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

        <link rel="shortcut icon" href="assets/img/about1.png" type="image/x-icon"/>

    </head>
    <body>
        <!--===== HEADER =====-->
        <header class="l-header">
            <nav class="nav bd-grid">
                <div>
                    <a href="/" class="nav__logo">Elena</a>
                    <span class="nav__subtitle">Full Stack Developer</span>
                </div>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item"><a href="#home" class="nav__link active">Home</a></li>
                        <li class="nav__item"><a href="#about" class="nav__link">About</a></li>
                        <li class="nav__item"><a href="#skills" class="nav__link">Skills</a></li>
                        <li class="nav__item"><a href="#work" class="nav__link">Work</a></li>
                        <li class="nav__item"><a href="#contact" class="nav__link">Contact</a></li>
                    </ul>
                </div>

                <div class="nav__toggle" id="nav-toggle">
                    <i class='bx bx-menu'></i>
                </div>
            </nav>
        </header>

        <main class="l-main">
            <!--===== HOME =====-->
            <section class="home bd-grid" id="home">
                <div class="home__data">
                <?php if(isset($_GET['Message'])){  echo $_REQUEST['Message']; } ?>
               <h1 class="home__title">Welcome To<br>My <span class="home__title-color">Portfolio</span><br> Website</h1>
                    <a href="https://www.facebook.com/differentdeveloper" target="__blank" class="button">Connect</a>
                </div>

                <div class="home__social">
                    <a href="https://www.linkedin.com/in/klisarovska/" class="home__social-icon"><i class='bx bxl-linkedin'></i></a>
                    <a href="https://gitlab.com/Klisarovska" class="home__social-icon"><i class='bx bxl-gitlab'></i></a>
                    <a href="https://github.com/Missellena" class="home__social-icon"><i class='bx bxl-github' ></i></a>
                </div>

                <div class="home__img">    
                    <img src="assets/img/imageonline-co-whitebackgroundremoved.png" alt="">
                </div>
            </section>

            <!--===== ABOUT =====-->
            <section class="about section " id="about">
                <h2 class="section-title">About</h2>

                <div class="about__container bd-grid">
                    <div class="about__img">
                        <img src="assets/img/about1.png" alt="">
                    </div>
                    
                    <div>
                        <h2 class="about__subtitle">I'am a Full Stack Developer</h2>
                        <p class="about__text">I can tackle  any project by building both the front and back end of a site or app simoultaneously. Essentially, we can work together on any web development project, discuss ideas, and I will create the design, execute the whole project and provide continuous support. <br/><br /> <b>Just contact me and we can have a chat about your ideas and see if we can work together.</b></p>           
                    </div>                                   
                </div>
            </section>

            <!--===== SKILLS =====-->
            <section class="skills section" id="skills">
                <h2 class="section-title">Skills</h2>

                <div class="skills__container bd-grid">          
                    <div>
                        <h2 class="skills__subtitle">Front-End Skills</h2>
                        <p class="skills__text">I build the visible parts of websites that users see and interact within their web browsers mostly with the following programming languages and technologies</p>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bxl-html5 skills__icon'></i>
                                <span class="skills__name">HTML5/SCSS</span>
                            </div>
                            <div class="skills__bar skills__html">

                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bxl-javascript skills__icon' ></i>
                                <span class="skills__name">JAVASCRIPT</span>
                            </div>
                            <div class="skills__bar skills__js">
                                
                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bxl-react skills__icon'></i>
                                <span class="skills__name">REACT.JS</span>
                            </div>
                            <div class="skills__bar skills__react">
                                
                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2 class="skills__subtitle">Back-End Skills</h2>
                        <p class="skills__text">I specialize in back-end development and I use these languages, libraries and frameworks to build the “non-visible” parts of websites that users don’t interact with directly.</p>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bx-code-alt skills__icon'></i>
                                <span class="skills__name">PHP</span>
                            </div>
                            <div class="skills__bar skills__php">

                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bx-cuboid skills__icon' ></i>
                                <span class="skills__name">LARAVEL</span>
                            </div>
                            <div class="skills__bar skills__laravel">
                                
                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bxl-wordpress skills__icon'></i>
                                <span class="skills__name">WORDPRESS</span>
                            </div>
                            <div class="skills__bar skills__wordpress">
                                
                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h2 class="skills__subtitle">Currently working with</h2>
                        <p class="skills__text">I always try to keep up with new technologies and aquire different skills. So currently I am working on:</p>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bx-code-alt skills__icon'></i>
                                <span class="skills__name">C#</span>
                            </div>
                            <div class="skills__bar skills__php">

                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                        <div class="skills__data">
                            <div class="skills__names">
                                <i class='bx bx-cuboid skills__icon' ></i>
                                <span class="skills__name">ASP.NET</span>
                            </div>
                            <div class="skills__bar skills__laravel">
                                
                            </div>
                            <div>
                                <span class="skills__percentage"><i class='bx bx-check-circle'></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--===== WORK =====-->
            <section class="work section" id="work">
                <h2 class="section-title">Work</h2>
                <div class="work__container bd-grid">
                    <div class="work__img">
                        <a href="#" target="__blank"><img src="assets/img/work1.png" alt=""></a>
                    </div>
                    <div class="work__img">
                        <a href="#" target="__blank"><img src="assets/img/work3.png" alt=""></a>
                    </div>
                    <div class="work__img">
                        <a href="#" target="__blank"><img src="assets/img/work2.png" alt=""></a>
                    </div>
                    <div class="work__img">
                        <a href="#" target="__blank"><img src="assets/img/work4.png" alt=""></a>
                    </div>
                    <div class="work__img">
                        <a href="#" target="__blank"><img src="assets/img/work5.png" alt=""></a>
                    </div>
                    <div class="work__img">
                    <a href="#" target="__blank"><img src="assets/img/work6.png" alt=""></a>
                    </div>
                    <div class="work__img">
                    <a href="#" target="__blank"><img src="assets/img/work7.png" alt=""></a>
                    </div>
                </div>
            </section>

            <!--===== CONTACT =====-->
            <section class="contact section" id="contact">
                <h2 class="section-title">Contact</h2>

                <div class="contact__container bd-grid">
                    <form action="mail.php" id="confirmationForm" class="contact__form" method="post">
                        <input type="text" placeholder="Name" class="contact__input" name="name">
                        <input type="mail" placeholder="Email" class="contact__input" name="email">
                        <textarea cols="0" rows="10" class="contact__input" name="body" form="confirmationForm"></textarea>
                        <input type="submit" value="Send" class="contact__button button">
                    </form>
                </div>
            </section>
        </main>

        <!--===== FOOTER =====-->
        <footer class="footer">
            <p class="footer__title">Elena</p>
            <p class="footer__subtitle">Full Stack Developer</p>
            <div class="footer__social">
                <a href="https://www.facebook.com/differentdeveloper" target="__blank"  class="footer__icon"><i class='bx bxl-facebook' ></i></a>
                <a href="#" class="footer__icon"><i class='bx bxl-instagram' ></i></a>
                <a href="#" class="footer__icon"><i class='bx bxl-twitter' ></i></a>
            </div>
            <p>&#169; 2020 copyright all right reserved</p>
        </footer>


        <!--===== SCROLL REVEAL =====-->
        <script src="https://unpkg.com/scrollreveal"></script>

        <!--===== MAIN JS =====-->
        <script src="assets/js/main.js"></script>
    </body>
</html>
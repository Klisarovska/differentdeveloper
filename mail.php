<?php 
function sanitize_my_email($field) {
    $field = filter_var($field, FILTER_SANITIZE_EMAIL);
    if (filter_var($field, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
$to_email = $_POST['email'];
$subject = $_POST['name'];
$body = $_POST['body'];
$headers = 'From: noreply @ differentdeveliper . com';
$secure_check = sanitize_my_email($to_email);
    if ($secure_check == false) {
        $Message = "Invalid input!";
        header('Location: ' . $_SERVER['HTTP_REFERER'].'?Message='.$Message);
    } else { 
        mail($to_email, $subject, $body, $headers);
        $Message = 'Email was sent!';
        header('Location: ' . $_SERVER['HTTP_REFERER'].'?Message='.$Message);
    }
} else {
    echo "Get method not supported";
}
?>